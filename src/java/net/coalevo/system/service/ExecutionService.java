/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.service;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.ConfigurableService;

import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Provides a {@link net.coalevo.foundation.model.Service} for
 * asynchronous execution of tasks.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface ExecutionService
    extends ConfigurableService {

  /**
   * Executes the given <tt>Runnable</tt> at some
   * time in the future.
   *
   * @param a    the requesting {@link Agent}.
   * @param task a <tt>Runnable</tt>.
   */
  public void execute(Agent a, Runnable task);

  /**
   * Submits the given <tt>Runnable</tt> for system and
   * returns a <tt>Future</tt> representing that task.
   *
   * @param a    the requesting {@link Agent}.
   * @param task a <tt>Runnable</tt>.
   * @return a <tt>Future</tt> representing the task.
   */
  public Future submit(Agent a, Runnable task);

  /**
   * Removes the given <tt>Runnable</tt> from the internal queue if
   * it is still present (i.e. has not been run yet). Calling this
   * method makes only sense for tasks that have been scheduled
   * with {@link #execute(Agent,Runnable)}. In all other cases the
   * returned <tt>Future</tt> provides the means for cancellation.
   *
   * @param a    the requesting {@link Agent}.
   * @param task a <tt>Runnable</tt>.
   * @return true if removed, false otherwise.
   */
  public boolean remove(Agent a, Runnable task);

  /**
   * Schedules the given <tt>Runnable</tt> for one-time system
   * after a given delay.
   *
   * @param a     the requesting {@link Agent}.
   * @param task  a <tt>Runnable</tt>.
   * @param delay the delay as <tt>long</tt>.
   * @param unit  a <tt>TimeUnit</tt> instance specifying the time unit of the delay.
   * @return a <tt>ScheduledFuture</tt> representing the task.
   */
  public ScheduledFuture schedule(Agent a, Runnable task, long delay, TimeUnit unit);

  /**
   * Schedules a given <tt>Runnable</tt> for periodical system after the
   * initial delay.
   *
   * @param a            the requesting {@link Agent}.
   * @param task         a <tt>Runnable</tt>.
   * @param initialDelay the initial delay as <tt>long</tt>.
   * @param period       the period for system as <tt>long</tt>.
   * @param unit         a <tt>TimeUnit</tt> instance specifying the time unit
   *                     of initialDelay and period.
   * @return a <tt>ScheduledFuture</tt> representing the task.
   */
  public ScheduledFuture scheduleAtFixedRate(Agent a,
                                             Runnable task,
                                             long initialDelay,
                                             long period,
                                             TimeUnit unit);


}//interface ExecutionService
