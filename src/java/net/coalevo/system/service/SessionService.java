/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.service;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.Service;
import net.coalevo.foundation.model.Session;
import net.coalevo.foundation.model.UserAgent;

import java.util.concurrent.TimeUnit;

/**
 * Provides a {@link net.coalevo.foundation.model.Service} for
 * session handling in stateless contexts (e.g. HTTP, Web Services etc.).
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface SessionService extends Service {

  /**
   * Initiates a new session for the given {@link UserAgent}.
   * If a {@link UserAgent} instance is provided, for which a
   * {@link Session} exists, a reference to the existing {@link Session}
   * instance will be returned.
   *
   * @param a       an {@link UserAgent} instance.
   * @param maxidle the maximum idle time value.
   * @param unit    the time unit of the max. idle time.
   * @return a {@link Session} instance.
   * @throws SecurityException if the given {@link Agent} is not authentic.
   */
  public Session initiateSession(UserAgent a, long maxidle, TimeUnit unit)
      throws SecurityException;

  /**
   * Initiates a new session for a {@link UserAgent} specified
   * through the given id/password pair.
   * <p/>
   * This is a convenience method that represents a shortcut
   * for authentication of an {@link UserAgent} through this
   * service.
   * </p>
   *
   * @param agentid  an identifier of an agent as <tt>String</tt>.
   * @param password the credential as <tt>String</tt>.
   * @param maxidle  the maximum idle time value.
   * @param unit     the time unit of the max. idle time.
   * @return a {@link Session} instance.
   * @throws SecurityException if the given id/password pair does not
   *                           authenticate a {@link UserAgent}.
   */
  public Session initiateSession(String agentid, String password, long maxidle, TimeUnit unit);

  /**
   * Returns the session bound to the given identifier.
   *
   * @param identifier a session identifier.
   * @return the {@link Session} instance bound to the
   *         given identifier.
   */
  public Session getSession(String identifier);

}//interface SessionService
