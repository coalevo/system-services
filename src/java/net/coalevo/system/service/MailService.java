/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.service;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.ConfigurableService;

/**
 * Defines the contract for a simple mail service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MailService
    extends ConfigurableService {

  /**
   * Sends mail to a given recipient.
   * <p/>
   * Note that the implementation should handle sending asynchronously,
   * e.g. this method should return immediately after verfication and queueing.
   * </p>
   *
   * @param caller    the calling {@link Agent}.
   * @param recipient a String representing a valid email address.
   * @param fn        a realname of the recipient.
   * @param subject   the subject of the mail.
   * @param body      the body of the mail.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or authorized.
   * @throws MailServiceException if the recipient is not valid.
   */
  public void send(Agent caller, String recipient, String fn, String subject, String body)
      throws SecurityException, MailServiceException;

  /**
   * Sends mail to a list of recipients.
   * <p/>
   * Note that an implementation should not send these mails as
   * carbon copies, but rather queue single mails to all specified recipients
   * to prevent accumulation of mail addresses for spamming.
   * This may be a convenience method that calls {@link #send(Agent,String,String,String,String)}
   * </p>
   * <p/>
   * Note that the implementation should handle sending asynchronously,
   * e.g. this method should return immediately after verfication and queueing.
   * </p>
   *
   * @param caller     the calling {@link Agent}.
   * @param recipients a String[] representing a list of valid email addresses.
   * @param fn         a String[] representing a list of realnames corresponding to each recipient.
   * @param subject    the subject of the mail.
   * @param body       the body of the mail.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or authorized.
   * @throws MailServiceException if a recipient is not valid.
   */
  public void send(Agent caller, String[] recipients, String[] fn, String subject, String body)
      throws SecurityException, MailServiceException;

}//class MailService
