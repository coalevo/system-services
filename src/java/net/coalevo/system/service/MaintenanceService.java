/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.service;

import net.coalevo.foundation.model.Service;
import net.coalevo.foundation.model.ConfigurableService;
import net.coalevo.foundation.model.Maintainable;
import net.coalevo.foundation.model.Agent;

/**
 * Provides a {@link Service} that
 * will automatically invoke maintenance for all registered
 * {@link net.coalevo.foundation.model.Maintainable} instances
 * (OSGi white-board model).
 * <p/>
 * Configuration is managed through the standard OSGi configuration
 * mechanism.
 * <p/>
 * <em>No implementation available yet!</em>
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MaintenanceService
    extends ConfigurableService {

  /**
   * Asynchronously executes the maintenance routines in all registered
   * {@link Maintainable}s.
   *
   * @param caller the calling {@link Agent}.
   */
  public void doMaintenance(Agent caller);
    
}//interface MaintenanceServiceImpl
