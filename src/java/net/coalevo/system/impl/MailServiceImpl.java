/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.impl;

import net.coalevo.foundation.model.Action;
import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.BaseService;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.system.service.ExecutionService;
import net.coalevo.system.service.MailService;
import net.coalevo.system.service.MailServiceException;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Dictionary;
import java.util.Properties;

/**
 * Provides an implementation of {@link MailService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MailServiceImpl
    extends BaseService
    implements MailService {

  private Marker m_LogMarker = MarkerFactory.getMarker(MailServiceImpl.class.getName());
  private BundleContext m_BundleContext;

  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  private boolean m_Active = false;
  private Messages m_BundleMessages;

  /**
   * Mail Settings for sending email.
   */
  private String m_MailHost;
  private int m_MailPort;
  private boolean m_MailSecure;
  private String m_MailFrom;
  private String m_MailFromRealname;

  private boolean m_MailAuthenticated;
  private String m_MailUser;
  private String m_MailPassword;


  public MailServiceImpl() {
    super(MailService.class.getName(), ACTIONS);
  }//constructor

  public boolean activate(BundleContext bc) {
    m_BundleContext = bc;
    ServiceMediator services = Activator.getServices();
    m_BundleMessages = Activator.getBundleMessages();
    //1. Check if active
    if (m_Active) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.activate.active", "service", "ExecutionService"));
      return false;
    }

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/MailService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);

    //4. Register service
    final Properties p = new Properties();
    p.put(org.osgi.framework.Constants.SERVICE_PID, getIdentifier());
    String[] classes = new String[]{MailService.class.getName(), ManagedService.class.getName()};
    m_BundleContext.registerService(classes,
        this,
        p);

    //5. Make sure that some config is available for editing
    ConfigurationAdmin ca = services.getConfigurationAdmin(ServiceMediator.WAIT_UNLIMITED);
    if (ca == null) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("MailServiceImpl.config.noconfigadmin"));
      return false;
    }
    try {
      Configuration[] c = ca.listConfigurations("(" + org.osgi.framework.Constants.SERVICE_PID + "=" + getIdentifier() + ")");
      if (c == null || c.length == 0) {
        Configuration config = ca.getConfiguration(getIdentifier());
        //update with defaults
        config.update(new MetaTypeDictionary(null, m_BundleContext, getIdentifier()).getDictionary());
        Activator.log().info(m_LogMarker, m_BundleMessages.get("MailServiceImpl.config.initialized"));
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      Activator.log().error(m_LogMarker, "activate()", ex);
      return false;
    }


    m_Active = true;
    return true;
  }//activate

  public boolean deactivate() {
    m_Active = false;
    if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }
    m_BundleMessages = null;
    m_BundleContext = null;

    return true;
  }//deactivate

  //This will be called by a ConfigurationAdmin thread
  public void updated(Dictionary conf)
      throws ConfigurationException {
    //System.err.println("updated(Dictionary)" + ((www == null) ? "NULL" : www.toString()));
    Activator.log().debug(m_LogMarker, "updated(Dictionary)" + ((conf == null) ? "NULL" : conf.toString()));
    try {
      //Note: if the www is null, then the dictionary will automatically return
      //defaults for all values.
      final MetaTypeDictionary config = new MetaTypeDictionary(conf, m_BundleContext, getIdentifier());
      config.getDictionary();
      try {
        m_MailHost = config.getString(CONFIG_SMTP_HOST);
        m_MailPort = config.getInteger(CONFIG_SMTP_PORT).intValue();
        m_MailSecure = config.getBoolean(CONFIG_SMTP_SECURE).booleanValue();
        m_MailAuthenticated = config.getBoolean(CONFIG_SMTP_AUTHENTICATED).booleanValue();
        if (m_MailAuthenticated) {
          m_MailUser = config.getString(CONFIG_SMTP_USER);
          m_MailPassword = config.getString(CONFIG_SMTP_PASSWORD);
        }
        m_MailFrom = config.getString(CONFIG_MAIL_SENDER_ADDRESS);
        m_MailFromRealname = config.getString(CONFIG_MAIL_SENDER_REALNAME);
      } catch (MetaTypeDictionaryException ex) {
        Activator.log().error(m_LogMarker, "updated()", ex);
      }
    } catch (UnsupportedOperationException ex) {
      final String msg = m_BundleMessages.get("ExecutionServiceImpl.config.nometatype");
      Activator.log().error(m_LogMarker, msg, ex);
      throw new ConfigurationException("dictionary", msg, ex);
    }
  }//update


  public void send(Agent caller, final String recipient, final String fn, final String subject, final String body)
      throws SecurityException, MailServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), SEND);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", SEND.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return;
    }

    ServiceMediator services = Activator.getServices();
    ExecutionService exs = services.getExecutionService(ServiceMediator.NO_WAIT);

    exs.execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
      public void run() {
        //1. set bundle class loader
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(Activator.class.getClassLoader());

        try {
          doSend(recipient, fn, subject, body);
        } catch (Exception mex) {
          Activator.log().error(m_LogMarker, "send()", mex);
          mex.printStackTrace(System.err);
        } finally {
          Thread.currentThread().setContextClassLoader(cl);
        }
      }//run
    });
  }//send

  public void send(Agent caller, final String[] recipients, final String[] fn,
                   final String subject, final String body)
      throws SecurityException, MailServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), SEND);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", SEND.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return;
    }

    ServiceMediator services = Activator.getServices();
    ExecutionService exs = services.getExecutionService(ServiceMediator.NO_WAIT);

    final Logger log = Activator.log();
    exs.execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
      public void run() {
        //1. set bundle class loader
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(getClass().getClassLoader());

        try {
          for (int i = 0; i < recipients.length; i++) {
            doSend(recipients[i], fn[i], subject, body);
          }
        } catch (Exception mex) {
          log.error(m_LogMarker, "send()", mex);
        } finally {
          Thread.currentThread().setContextClassLoader(cl);
        }
      }//run
    });
  }//send

  private void doSend(final String recipient, final String fn,
                      final String subject, final String body)
      throws SecurityException, EmailException {

    SimpleEmail email = new SimpleEmail();
    email.setDebug(DEBUG);
    email.setHostName(m_MailHost);
    email.setSmtpPort(m_MailPort);
    if (m_MailSecure) {
      email.setSSL(true);
    }
    if (m_MailAuthenticated) {
      email.setAuthentication(m_MailUser, m_MailPassword);
    }
    email.addTo(recipient, fn);
    email.setFrom(m_MailFrom, m_MailFromRealname);
    email.setSubject(subject);
    email.setMsg(body);
    email.send();
    Activator.log().info(m_LogMarker, "Sent mail to " + fn + " <" + recipient + ">" + "\"" + subject + "\"");
  }//send

  private static Action SEND = new Action("send");

  private static Action[] ACTIONS = {
      SEND
  };

  private static final String CONFIG_SMTP_HOST = "mail.smtp.host";
  private static final String CONFIG_SMTP_PORT = "mail.smtp.port";
  private static final String CONFIG_SMTP_AUTHENTICATED = "mail.smtp.authenticated";
  private static final String CONFIG_SMTP_SECURE = "mail.smtp.secure";
  private static final String CONFIG_SMTP_USER = "mail.smtp.user";
  private static final String CONFIG_SMTP_PASSWORD = "mail.smtp.password";
  private static final String CONFIG_MAIL_SENDER_ADDRESS = "mail.sender.address";
  private static final String CONFIG_MAIL_SENDER_REALNAME = "mail.sender.fn";
  private static final boolean DEBUG = new Boolean(System.getProperty("mail.debug", "false"));

}//class MailServiceImpl
