/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.impl;

import net.coalevo.foundation.model.Messages;
import net.coalevo.logging.model.LogProxy;
import net.coalevo.system.service.ExecutionService;
import net.coalevo.system.service.MailService;
import net.coalevo.system.service.MaintenanceService;
import net.coalevo.system.service.SessionService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides the <tt>BundleActivator</tt> implementation
 * for the system services bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {


  private static Marker c_LogMarker;
  private static LogProxy c_Log;

  private static Messages c_BundleMessages;
  private static ServiceMediator c_Services;

  private ExecutionService m_ExecutionService;
  private MailService m_MailService;
  private SessionService m_SessionService;
  private MaintenanceService m_MaintenanceService;

  private Thread m_StartThread;

  public void start(final BundleContext bundleContext) throws Exception {
    if (m_StartThread != null && m_StartThread.isAlive()) {
      throw new Exception();
    }
    m_StartThread = new Thread(new Runnable() {

      public void run() {
        try {

          //1. Log
          c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
          c_Log = new LogProxy();
          c_Log.activate(bundleContext);

          //2. Services
          c_Services = new ServiceMediator();
          c_Services.activate(bundleContext);

          //3. Bundle Messages
          c_BundleMessages =
              c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED)
                  .getBundleMessages(bundleContext.getBundle());

          //4. Bundle provided services
          // ExecutionService
          m_ExecutionService = new ExecutionServiceImpl();
          if (!m_ExecutionService.activate(bundleContext)) {
            log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "ExecutionService"));
          }
          log().info(c_BundleMessages.get("Activator.activation.service", "service", "ExecutionService"));
          //Session Service
          m_SessionService = new SessionServiceImpl();
          if (!m_SessionService.activate(bundleContext)) {
            log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "SessionService"));
            return;
          }
          bundleContext.registerService(
              SessionService.class.getName(),
              m_SessionService,
              null);
          log().info(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "SessionService"));

          //MailService
          m_MailService = new MailServiceImpl();
          if (!m_MailService.activate(bundleContext)) {
            log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "MailService"));
          }
          log().info(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "MailService"));

          //MaintenanceService
          m_MaintenanceService = new MaintenanceServiceImpl();
          if (!m_MaintenanceService.activate(bundleContext)) {
            log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "MaintenanceService"));
          }
          log().info(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "MaintenanceService"));


          log().info(c_LogMarker, c_BundleMessages.get("Activator.started"));
        } catch (Exception ex) {
          log().error("start()", ex);
        }
      }//run
    }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext) throws Exception {
    //wait start
    if (m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }
    if (m_SessionService != null) {
      m_SessionService.deactivate();
      m_SessionService = null;
    }
    if (m_ExecutionService != null) {
      m_ExecutionService.deactivate();
      m_ExecutionService = null;
    }
    if (m_MailService != null) {
      m_MailService.deactivate();
      m_MailService = null;
    }
    if (m_MaintenanceService != null) {
      m_MaintenanceService.deactivate();
      m_MaintenanceService = null;
    }
    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }
    if (c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }
    c_LogMarker = null;
    c_BundleMessages = null;
  }//stop

  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

}//class Activator