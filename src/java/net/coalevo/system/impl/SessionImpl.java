/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.impl;

import net.coalevo.foundation.model.Session;
import net.coalevo.foundation.model.SessionListener;
import net.coalevo.foundation.model.UserAgent;
import org.apache.commons.collections.list.CursorableLinkedList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Provides an implementation of {@link Session}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class SessionImpl
    implements Session {

  private AtomicBoolean m_Valid;
  private CountDownLatch m_Invalidating;
  private final SessionServiceImpl m_SessionService;
  private final long m_CreationTime;
  private final String m_Identifier;
  private final UserAgent m_UserAgent;
  private long m_MaxIdleTime;
  private long m_LastAccessed;
  private HashMap m_Attributes;
  private CursorableLinkedList m_Listeners;
  private boolean m_TimedOut;

  public SessionImpl(SessionServiceImpl s, UserAgent a, long maxidle, TimeUnit unit) {
    long now = System.currentTimeMillis();
    m_CreationTime = now;
    m_LastAccessed = now;
    setMaxIdleTime(maxidle, unit);
    m_Identifier = s.getNewSessionId();
    m_UserAgent = a;
    m_Attributes = new HashMap();
    m_SessionService = s;
    m_Valid = new AtomicBoolean(true);
    m_Listeners = new CursorableLinkedList();
  }//constructor

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public long getCreationTime()
      throws IllegalStateException {
    return m_CreationTime;
  }//getCreationTime

  protected long getAge() {
    return System.currentTimeMillis() - m_CreationTime;
  }//getAge

  public long getLastAccessedTime() {
    return m_LastAccessed;
  }//getLastAccessedTime

  private void setMaxIdleTime(long maxidle, TimeUnit unit) {
    m_MaxIdleTime = unit.toMillis(maxidle);
  }//setMaxIdleTime

  public long getMaxIdleTime() {
    return m_MaxIdleTime;
  }//getMaxIdleTime

  protected long getIdleTime() {
    return System.currentTimeMillis() - m_LastAccessed;
  }//getIdleTime

  public void invalidate() {
    if (m_Valid.getAndSet(false)) {
      m_Invalidating = new CountDownLatch(1);
      m_SessionService.endSession(this, m_Invalidating);
    }
  }//invalidate

  public void invalidate(boolean wait) {
    if (m_Valid.getAndSet(false)) {
      m_Invalidating = new CountDownLatch(1);
      m_SessionService.endSession(this, m_Invalidating);
      if (wait) {
        try {
          m_Invalidating.await();
        } catch (InterruptedException ie) {

        }
      }
    }
  }//invalidate

  public boolean isValid() {
    return m_Valid.get();
  }//isValid

  public UserAgent getAgent()
      throws IllegalStateException {
    ensureValid();
    return m_UserAgent;
  }//getAgent

  public Object getAttribute(String name)
      throws IllegalStateException {
    ensureValid();
    return m_Attributes.get(name);
  }//getAttributes

  public void setAttribute(String name, Object value)
      throws IllegalStateException {
    ensureValid();
    m_Attributes.put(name, value);
  }//setAttribute

  public void removeAttribute(String name)
      throws IllegalStateException {
    ensureValid();
    m_Attributes.remove(name);
  }//removeAttribute

  public Iterator getAttributeNames()
      throws IllegalStateException {
    ensureValid();
    return m_Attributes.keySet().iterator();
  }//getAttributeNames

  public boolean hasAttribute(String name) throws IllegalStateException {
    ensureValid();
    return m_Attributes.containsKey(name);
  }//hasAttribute

  public void addSessionListener(SessionListener l) {
    m_Listeners.add(l);
  }//addSessionListener

  public void removeSessionListener(SessionListener l) {
    m_Listeners.remove(l);
  }//removeSessionListener

  public Iterator getSessionListeners() {
    return m_Listeners.listIterator();
  }//notifySessionListeners

  private void ensureValid() throws IllegalStateException {
    if (!m_Valid.get()) {
      if (m_Invalidating != null && m_Invalidating.getCount() > 0) {
        return;
      } else {
        throw new IllegalStateException();
      }
    }
  }//ensureValid

  public void access() throws IllegalStateException {
    if (!m_Valid.get()) {
      throw new IllegalStateException();
    } else {
      m_LastAccessed = System.currentTimeMillis();
    }
  }//access

  public void invalidateIfTimedOut() {
    if (System.currentTimeMillis() - m_LastAccessed >= getMaxIdleTime()) {
      m_TimedOut = true;
      invalidate();
    }
  }//invalidateIfTimedOut

  public boolean isTimedOut() {
    return m_TimedOut;
  }//isTimedOut

  public String toString() {
    final StringBuilder sbuf = new StringBuilder("Session {");
    sbuf.append(m_UserAgent.getIdentifier());
    sbuf.append(",");
    sbuf.append(m_Identifier);
    sbuf.append(",");
    sbuf.append((m_Valid.get()) ? "valid," : "invalid,");
    sbuf.append("age ");
    sbuf.append(TimeUnit.MILLISECONDS.toSeconds(getAge()));
    sbuf.append(" s,");

    sbuf.append("idle ");
    sbuf.append(TimeUnit.MILLISECONDS.toSeconds(getIdleTime()));
    sbuf.append(" s");

    sbuf.append("maxidle ");
    sbuf.append(TimeUnit.MILLISECONDS.toSeconds(getMaxIdleTime()));
    sbuf.append(" s");

    sbuf.append('}');

    return sbuf.toString();
  }//toString

/*
  public static void main(String[] args) {
    SessionImpl s = new SessionImpl(null, new Agent() {

      public String getIdentifier() {
        return "dieter@coalevo.net";
      }
    });
    System.out.println(s.toString());
    synchronized(s) {
      try {
        s.wait(10000);
      } catch (InterruptedException e) {
        e.printStackTrace(System.err);
      }
    }
    System.out.println(s.toString());
    s.access();
    System.out.println(s.toString());
  }
*/

}//class SessionImpl
