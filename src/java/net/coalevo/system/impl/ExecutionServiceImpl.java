/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.impl;

import net.coalevo.foundation.model.Action;
import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.BaseService;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.system.service.ExecutionService;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Dictionary;
import java.util.Properties;
import java.util.concurrent.*;

/**
 * Provides an system service based on the concurrent classes
 * provided by <tt>java.util.concurrent</tt> or it's backport.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ExecutionServiceImpl
    extends BaseService
    implements ExecutionService {

  private Marker m_LogMarker = MarkerFactory.getMarker(ExecutionServiceImpl.class.getName());
  private BundleContext m_BundleContext;

  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  private ScheduledThreadPoolExecutor m_Executor;
  private boolean m_Active = false;
  private Messages m_BundleMessages;

  public ExecutionServiceImpl() {
    super(ExecutionService.class.getName(), ACTIONS);
  }//constructor

  public boolean activate(BundleContext bc) {
    m_BundleContext = bc;
    ServiceMediator services = Activator.getServices();
    m_BundleMessages = Activator.getBundleMessages();
    //1. Check if active
    if (m_Active) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.activate.active", "service", "ExecutionService"));
      return false;
    }

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/ExecutionService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);

    //Prepare Executor
    m_Executor = new ScheduledThreadPoolExecutor(
        5, new ExecutionThreadFactory()
    );
    m_Executor.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
    m_Executor.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);

    //4. Register service
    final Properties p = new Properties();
    p.put(org.osgi.framework.Constants.SERVICE_PID, getIdentifier());
    String[] classes = new String[]{ExecutionService.class.getName(), ManagedService.class.getName()};
    m_BundleContext.registerService(classes,
        this,
        p);

    //5. Make sure that some config is available for editing
    ConfigurationAdmin ca = services.getConfigurationAdmin(ServiceMediator.WAIT_UNLIMITED);
    if (ca == null) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("ExecutionServiceImpl.config.noconfigadmin"));
      return false;
    }
    try {
      Configuration[] c = ca.listConfigurations("(" + org.osgi.framework.Constants.SERVICE_PID + "=" + getIdentifier() + ")");
      if (c == null || c.length == 0) {
        Configuration config = ca.getConfiguration(getIdentifier());
        //update with defaults
        config.update(new MetaTypeDictionary(null, m_BundleContext, getIdentifier()).getDictionary());
        Activator.log().info(m_LogMarker, m_BundleMessages.get("ExecutionServiceImpl.config.initialized"));
      }
    } catch (Exception ex) {
      Activator.log().error("activate()", ex);
      return false;
    }

    m_Active = true;
    return true;
  }//activate

  public boolean deactivate() {
    m_Active = false;

    if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }

    m_BundleMessages = null;
    m_Executor = null;
    m_BundleContext = null;
    return true;
  }//deactivate

  //This will be called by a ConfigurationAdmin thread
  public void updated(Dictionary conf)
      throws ConfigurationException {
    //System.err.println("updated(Dictionary)" + ((www == null) ? "NULL" : www.toString()));
    Activator.log().debug(m_LogMarker, "updated(Dictionary)" + ((conf == null) ? "NULL" : conf.toString()));
    try {
      //Note: if the www is null, then the dictionary will automatically return
      //defaults for all values.
      final MetaTypeDictionary config = new MetaTypeDictionary(conf, m_BundleContext, getIdentifier());
      config.getDictionary();
      try {
        int nsize = config.getInteger(CONFIG_THREADPOOLSIZE_KEY).intValue();
        m_Executor.setCorePoolSize(nsize);
      } catch (MetaTypeDictionaryException ex) {
        final String msg = m_BundleMessages.get("ExecutionServiceImpl.config.attribute", "name", CONFIG_THREADPOOLSIZE_KEY);
        Activator.log().error(m_LogMarker, msg, ex);
        throw new ConfigurationException(CONFIG_THREADPOOLSIZE_KEY, msg, ex);
      }
    } catch (UnsupportedOperationException ex) {
      final String msg = m_BundleMessages.get("ExecutionServiceImpl.config.nometatype");
      Activator.log().error(m_LogMarker, msg, ex);
      throw new ConfigurationException("dictionary", msg, ex);
    }
  }//update

  public void execute(Agent caller, Runnable task) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), EXECUTE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", EXECUTE.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    m_Executor.execute(task);
  }//execute

  public Future submit(Agent caller, Runnable task) {
    try {

      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), SUBMIT);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", SUBMIT.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_Executor.submit(task);
  }//submit

  public boolean remove(Agent caller, Runnable task) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REMOVE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", REMOVE.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_Executor.remove(task);
  }//remove

  public ScheduledFuture schedule(Agent caller,
                                  Runnable task,
                                  long delay,
                                  TimeUnit unit) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), SCHEDULE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", SCHEDULE.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_Executor.schedule(task, delay, unit);
  }//schedule

  public ScheduledFuture scheduleAtFixedRate(Agent caller,
                                             Runnable task,
                                             long initialDelay,
                                             long period,
                                             TimeUnit unit) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), SCHEDULE_RATE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", SCHEDULE_RATE.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_Executor.scheduleAtFixedRate(task, initialDelay, period, unit);
  }//scheduleAtFixedRate

  private static Action EXECUTE = new Action("execute");
  private static Action SUBMIT = new Action("submit");
  private static Action REMOVE = new Action("remove");
  private static Action SCHEDULE = new Action("schedule");
  private static Action SCHEDULE_RATE = new Action("scheduleAtFixedRate");

  private static Action[] ACTIONS = {
      EXECUTE, SUBMIT, REMOVE, SCHEDULE, SCHEDULE_RATE
  };

  static final class ExecutionThreadFactory
      implements ThreadFactory {

    public Thread newThread(Runnable runnable) {
      return new Thread(runnable);
    }//newThread

  }//inner class ExecutionThreadFactory

  private static final String CONFIG_THREADPOOLSIZE_KEY = "threadpool.size";

}//class ExecutionServiceImpl
