/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.impl;

import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.service.UUIDGeneratorService;
import net.coalevo.system.service.ExecutionService;
import org.osgi.framework.*;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.metatype.MetaTypeService;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 * Provides a mediator for required coalevo services.
 * Allows to obtain fresh and latest references by using
 * the whiteboard model to track the services at all times.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceMediator {

  private Marker m_LogMarker = MarkerFactory.getMarker(ServiceMediator.class.getName());
  private BundleContext m_BundleContext;

  private UUIDGeneratorService m_UUIDGeneratorService;
  private MetaTypeService m_MetaTypeService;
  private ExecutionService m_ExecutionService;
  private ConfigurationAdmin m_ConfigurationAdmin;
  private MessageResourceService m_MessageResourceService;

  private CountDownLatch m_MetaTypeServiceLatch;
  private CountDownLatch m_UUIDGeneratorLatch;
  private CountDownLatch m_MessageResourceServiceLatch;
  private CountDownLatch m_ConfigurationAdminLatch;
  private CountDownLatch m_ExecutionServiceLatch;

  public MessageResourceService getMessageResourceService(long wait) {
    try {
      if (wait < 0) {
        m_MessageResourceServiceLatch.await();
      } else if (wait > 0) {
        m_MessageResourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getMessageResourceService()", e);
    }
    return m_MessageResourceService;
  }//getMessageResourceService

  public ConfigurationAdmin getConfigurationAdmin(long wait) {
    try {
      if (wait < 0) {
        m_ConfigurationAdminLatch.await();
      } else if (wait > 0) {
        m_ConfigurationAdminLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getConfigurationAdmin()", e);
    }
    return m_ConfigurationAdmin;
  }//getConfigurationAdmin

  public MetaTypeService getMetaTypeService(long wait) {

    try {
      if (wait < 0) {
        m_MetaTypeServiceLatch.await();
      } else if (wait > 0) {
        m_MetaTypeServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getMetaTypeService()", e);
    }
    return m_MetaTypeService;
  }//getMetaTypeService

  public UUIDGeneratorService getUUIDGeneratorService(long wait) {
    try {
      if (wait < 0) {
        m_UUIDGeneratorLatch.await();
      } else if (wait > 0) {
        m_UUIDGeneratorLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getUUIDGeneratorService()", e);
    }
    return m_UUIDGeneratorService;
  }//getUUIDGeneratorService

  public ExecutionService getExecutionService(long wait) {
    try {
      if (wait < 0) {
        m_ExecutionServiceLatch.await();
      } else if (wait > 0) {
        m_ExecutionServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getExecutionService()", e);
    }
    return m_ExecutionService;
  }//getExecutionService

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch


  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;

    //prepare waits if required
    m_MetaTypeServiceLatch = createWaitLatch();
    m_UUIDGeneratorLatch = createWaitLatch();
    m_MessageResourceServiceLatch = createWaitLatch();
    m_ConfigurationAdminLatch = createWaitLatch();
    m_ExecutionServiceLatch = createWaitLatch();

    //prepare listener
    ServiceListener serviceListener = new ServiceListenerImpl();
    //prepare the filter
    String filter =
        "(|(|(|(|(objectclass=" + MessageResourceService.class.getName() + ")" +
            "(objectclass=" + ConfigurationAdmin.class.getName() + "))" +
            "(objectclass=" + MetaTypeService.class.getName() + "))" +
            "(objectclass=" + ExecutionService.class.getName() + "))" +
            "(objectclass=" + UUIDGeneratorService.class.getName() + "))";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    //null out the references
    m_MetaTypeService = null;
    m_UUIDGeneratorService = null;
    m_MessageResourceService = null;
    m_ExecutionService = null;
    m_ConfigurationAdmin = null;

    if(m_MetaTypeServiceLatch != null) {
      m_MetaTypeServiceLatch.countDown();
      m_MetaTypeServiceLatch = null;
    }
    if(m_UUIDGeneratorLatch != null) {
      m_UUIDGeneratorLatch.countDown();
      m_UUIDGeneratorLatch = null;
    }
    if(m_MessageResourceServiceLatch != null) {
      m_MessageResourceServiceLatch.countDown();
      m_MessageResourceServiceLatch = null;
    }
    if(m_ExecutionServiceLatch != null) {
      m_ExecutionServiceLatch.countDown();
      m_ExecutionServiceLatch = null;
    }
    if(m_ConfigurationAdminLatch != null) {
      m_ConfigurationAdminLatch.countDown();
      m_ConfigurationAdminLatch = null;
    }

    m_LogMarker = null;
    m_BundleContext = null;
  }//deactivate

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof UUIDGeneratorService) {
            m_UUIDGeneratorService = (UUIDGeneratorService) o;
            m_UUIDGeneratorLatch.countDown();
          } else if (o instanceof MetaTypeService) {
            m_MetaTypeService = (MetaTypeService) o;
            m_MetaTypeServiceLatch.countDown();
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = (MessageResourceService) o;
            m_MessageResourceServiceLatch.countDown();
          } else if (o instanceof ConfigurationAdmin) {
            m_ConfigurationAdmin = (ConfigurationAdmin) o;
            m_ConfigurationAdminLatch.countDown();
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = (ExecutionService) o;
            m_ExecutionServiceLatch.countDown();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof UUIDGeneratorService) {
            m_UUIDGeneratorService = null;
            m_UUIDGeneratorLatch = createWaitLatch();
          } else if (o instanceof MetaTypeService) {
            m_MetaTypeService = null;
            m_MetaTypeServiceLatch = createWaitLatch();
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = null;
            m_MessageResourceServiceLatch = createWaitLatch();
          } else if (o instanceof ConfigurationAdmin) {
            m_ConfigurationAdmin = null;
            m_ConfigurationAdminLatch = createWaitLatch();
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = null;
            m_ExecutionServiceLatch = createWaitLatch();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;

}//class ServiceMediator
