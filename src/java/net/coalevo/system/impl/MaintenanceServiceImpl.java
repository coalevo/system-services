/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.system.service.ExecutionService;
import net.coalevo.system.service.MaintenanceService;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.*;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * This class implements {@link MaintenanceService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MaintenanceServiceImpl
    extends BaseService
    implements MaintenanceService {

  private Marker m_LogMarker = MarkerFactory.getMarker(MaintenanceServiceImpl.class.getName());
  private BundleContext m_BundleContext;

  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  private boolean m_Active = false;
  private Messages m_BundleMessages;
  private ScheduledFuture m_Scheduled;

  private MaintainableManager m_MaintainableManager;

  public MaintenanceServiceImpl() {
    super(MaintenanceService.class.getName(), ACTIONS);
  }//constructor

  public boolean activate(BundleContext bc) {
    m_BundleContext = bc;
    ServiceMediator services = Activator.getServices();
    m_BundleMessages = Activator.getBundleMessages();
    //1. Check if active
    if (m_Active) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.activate.active", "service", "MaintenanceService"));
      return false;
    }

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/MaintenanceService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);

    //4. Register service
    final Properties p = new Properties();
    p.put(org.osgi.framework.Constants.SERVICE_PID, getIdentifier());
    String[] classes = new String[]{MaintenanceService.class.getName(), ManagedService.class.getName()};
    m_BundleContext.registerService(classes,
        this,
        p);

    //5. Make sure that some config is available for editing
    ConfigurationAdmin ca = services.getConfigurationAdmin(ServiceMediator.WAIT_UNLIMITED);
    if (ca == null) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("MaintenanceServiceImpl.config.noconfigadmin"));
      return false;
    }
    try {
      Configuration[] c = ca.listConfigurations("(" + org.osgi.framework.Constants.SERVICE_PID + "=" + getIdentifier() + ")");
      if (c == null || c.length == 0) {
        Configuration config = ca.getConfiguration(getIdentifier());
        //update with defaults
        config.update(new MetaTypeDictionary(null, m_BundleContext, getIdentifier()).getDictionary());
        Activator.log().info(m_LogMarker, m_BundleMessages.get("MaintenanceServiceImpl.config.initialized"));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
      return false;
    }

    //Activate the manager
    m_MaintainableManager = new MaintainableManager();
    m_MaintainableManager.activate(bc);
    m_Active = true;
    return true;
  }//activate

  public boolean deactivate() {
    if (m_Scheduled != null) {
      try {
        m_Scheduled.cancel(true);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "deactivate()", ex);
      }
    }
    if (m_MaintainableManager != null) {
      m_MaintainableManager.deactivate();
    }

    if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }
    m_Active = false;
    m_BundleMessages = null;
    m_BundleContext = null;
    m_LogMarker = null;
    return true;
  }//deactivate

  //This will be called by a ConfigurationAdmin thread
  public void updated(Dictionary conf)
      throws ConfigurationException {
    Activator.log().debug(m_LogMarker, "updated(Dictionary)" + ((conf == null) ? "NULL" : conf.toString()));
    try {
      //Note: if the www is null, then the dictionary will automatically return
      //defaults for all values.
      final MetaTypeDictionary config = new MetaTypeDictionary(conf, m_BundleContext, getIdentifier());
      config.getDictionary();
      int starthour = 4;
      int periodhours = 24;
      try {
        starthour = config.getInteger(CONFIG_START_HOUR_KEY).intValue();
      } catch (MetaTypeDictionaryException ex) {
        final String msg = m_BundleMessages.get("MaintenanceServiceImpl.config.attribute", "name", CONFIG_START_HOUR_KEY);
        Activator.log().error(msg, ex);
        throw new ConfigurationException(CONFIG_START_HOUR_KEY, msg, ex);
      }
      try {
        periodhours = config.getInteger(CONFIG_PERIOD_HOURS_KEY).intValue();
      } catch (MetaTypeDictionaryException ex) {
        final String msg = m_BundleMessages.get("MaintenanceServiceImpl.config.attribute", "name", CONFIG_PERIOD_HOURS_KEY);
        Activator.log().error(msg, ex);
        throw new ConfigurationException(CONFIG_START_HOUR_KEY, msg, ex);
      }
      //Schedule maintenance
      scheduleMaintenance(starthour, periodhours);
    } catch (UnsupportedOperationException ex) {
      final String msg = m_BundleMessages.get("MaintenanceServiceImpl.config.nometatype");
      Activator.log().error(m_LogMarker, msg, ex);
      throw new ConfigurationException("dictionary", msg, ex);
    }
  }//update

  public void doMaintenance(final Agent caller) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), DO_MAINTENANCE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", DO_MAINTENANCE.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    ExecutionService es = Activator.getServices().getExecutionService(ServiceMediator.NO_WAIT);
    if (es == null) {
      Activator.log().error(m_LogMarker, "doMaintenance(Agent)::ExectionService unavailable.");
      return;
    }
    es.execute(
        caller,
        new Runnable() {
          public void run() {
            for (Iterator<Maintainable> iter = m_MaintainableManager.getMaintenables(); iter.hasNext();) {
              Maintainable m = iter.next();
              try {
                m.doMaintenance(caller);
              } catch (Exception ex) {
                Activator.log().error(m_LogMarker, "doMaintenance(Agent)", ex);
              }
            }
          }
        }
    );
  }//doMaintenance

  private void scheduleMaintenance(int to, int period) {
    //1. Cancel old schedule
    if (m_Scheduled != null) {
      try {
        m_Scheduled.cancel(false);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "scheduleMaintenance()", ex);
      }
    }
    //2. Schedule new
    ExecutionService es = Activator.getServices().getExecutionService(ServiceMediator.NO_WAIT);
    if (es == null) {
      Activator.log().error(m_LogMarker, "scheduleMaintenance()::ExectionService unavailable.");
      return;
    }
    // Prepare runable:
    Runnable task = new Runnable() {

      private Logger log = Activator.log();
      private Messages m_Messages = Activator.getBundleMessages();
      private Marker lmk = MarkerFactory.getMarker("MAINTENANCE");
      private ClassLoader cl = Activator.class.getClassLoader();

      public void run() {
        ClassLoader tcl = Thread.currentThread().getContextClassLoader();
        try {
          Thread.currentThread().setContextClassLoader(cl);
          log.info(lmk, m_Messages.get("MaintenanceServiceImpl.maintenance.start"));
          for (Iterator<Maintainable> iter = m_MaintainableManager.getMaintenables(); iter.hasNext();) {
            Maintainable m = iter.next();
            log.info(lmk, m_Messages.get("MaintenanceServiceImpl.maintenance.run", "maintainable", m.getClass().getName()));
            try {
              m.doMaintenance(m_ServiceAgentProxy.getAuthenticPeer());
            } catch (Exception ex) {
              log.error(m_LogMarker, "scheduleMaintenance()", ex);
            }
          }
          //schedule a garbage collection
          System.gc();
          log.info(lmk, m_Messages.get("MaintenanceServiceImpl.maintenance.end"));
        } catch (Exception ex) {
          ex.printStackTrace(System.err);
        } finally {
          Thread.currentThread().setContextClassLoader(tcl);
        }
      }
    };

    if (to == -1 && period == -1) {
      m_Scheduled = es.schedule(m_ServiceAgentProxy.getAuthenticPeer(), task, 0, TimeUnit.MILLISECONDS);
    } else {
      //Note: The period is assumed to be in hours and calculated into ms
      //Note2: The delay is calculated in ms
      long delay = calculateDelay(to);
      long rate = ((long) period) * 3600000;

      m_Scheduled = es.scheduleAtFixedRate(
          m_ServiceAgentProxy.getAuthenticPeer(),
          task,
          delay,
          rate,
          TimeUnit.MILLISECONDS
      );
      //log
      MessageAttributes attr = m_BundleMessages.leaseAttributes();
      attr.add("delay", Long.toString(delay));
      attr.add("futuredelay", Long.toString(m_Scheduled.getDelay(TimeUnit.MILLISECONDS)));
      attr.add("period", Long.toString(rate));
      attr.add("futuredone", Boolean.toString(m_Scheduled.isDone()));
      attr.add("futurecancelled", Boolean.toString(m_Scheduled.isCancelled()));
      Activator.log().debug(
          m_LogMarker,
          m_BundleMessages.get("MaintenanceServiceImpl.maintenance.scheduledfuture", attr)
      );
    }
  }//scheduleMaintenance

  private long calculateDelay(int to) {
    long now = System.currentTimeMillis();
    Calendar c = Calendar.getInstance();
    Date d = new Date(now);
    c.setTime(d);
    int hod = c.get(Calendar.HOUR_OF_DAY);
    int hd = 0;
    if (to > hod) {
      hd = to - hod;
    } else {
      hd = 24 - hod + to;
    }
    c.add(Calendar.HOUR_OF_DAY, hd);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    Activator.log().info(m_LogMarker, m_BundleMessages.get("MaintenanceServiceImpl.maintenance.scheduled", "date", c.getTime().toString()));
    return c.getTimeInMillis() - now;
  }//getDelay


  private static Action DO_MAINTENANCE = new Action("doMaintenance");

  private static Action[] ACTIONS = {
      DO_MAINTENANCE
  };

  private static String CONFIG_START_HOUR_KEY = "maintenance.start.hour";
  private static String CONFIG_PERIOD_HOURS_KEY = "maintenance.period.hours";

}//class MaintenanceServiceImpl
