/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.system.service.SessionService;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


/**
 * Provides an implementation of {@link SessionService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class SessionServiceImpl
    extends BaseService
    implements SessionService, Maintainable {

  private Marker m_LogMarker = MarkerFactory.getMarker(SessionServiceImpl.class.getName());

  private ServiceMediator m_Services;
  private Messages m_BundleMessages;
  private boolean m_Active;

  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  private Map<String, Session> m_Sessions;
  private ScheduledFuture m_EvictorFuture;
  private AgentIdentifierInstanceCache m_AIDCache;

  public SessionServiceImpl() {
    super(SessionService.class.getName(), ACTIONS);
    m_Sessions = new ConcurrentHashMap<String, Session>();
    m_AIDCache = new AgentIdentifierInstanceCache();
  }//constructor

  public boolean activate(BundleContext bc) {
    m_Services = Activator.getServices();
    m_BundleMessages = Activator.getBundleMessages();
    //1. Check if active
    if (m_Active) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.activate.active", "service", "SessionServiceImpl"));
      return false;
    }

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/SessionService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);

    scheduleEvictor();
    m_Active = true;
    return true;
  }//activate

  public boolean deactivate() {
    m_Active = false;
    if (m_EvictorFuture != null) {
      m_EvictorFuture.cancel(true);
      m_EvictorFuture = null;
    }
    if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }
    m_Services = null;
    m_BundleMessages = null;
    m_LogMarker = null;
    return true;
  }//deactivate

  public Session initiateSession(String agentid, String password, long maxidle, TimeUnit unit)
      throws SecurityException {
    AgentIdentifier aid = m_AIDCache.get(agentid);
    //ensure single session, check authentic before invalidating
    SessionImpl s = findAgentSession(aid);
    if (s != null && s.isValid()) {
      m_ServiceAgentProxy.getSecurityService().authenticate(agentid,password);
      s.invalidate(true);
    }
    //Authenticate obtaining user agent
    final UserAgent ua = m_ServiceAgentProxy.getSecurityService().authenticate(new AgentIdentifier(agentid), password);
    s = new SessionImpl(this, ua, maxidle, unit);
    ua.setSession(s); //bind agent and session
    m_Sessions.put(s.getIdentifier(), s);
    Activator.log().info("Initiated " + s.toString());
    return s;
  }//initiateSession

  public Session initiateSession(UserAgent a, long maxidle, TimeUnit unit)
      throws SecurityException {
    //1. Check if the agent is authentic
    if (!m_ServiceAgentProxy.getSecurityService().isAuthentic(a)) {
      throw new SecurityException();
    }
    //2. Check if the agent has a session bound to it and invalidate it
    SessionImpl s = (SessionImpl) a.getSession();
    if (s != null && s.isValid()) {
      s.invalidate(true);
    }
    //3. Create a new session and bind it to the agent
    s = new SessionImpl(this, a, maxidle, unit);
    a.setSession(s); //bind agent and session
    m_Sessions.put(s.getIdentifier(), s);
    Activator.log().info(m_LogMarker, "Initiated " + s.toString());
    return s;
  }//initiateSession

  public Session getSession(String identifier) {
    Object o = m_Sessions.get(identifier);
    if (o == null) {
      throw new NoSuchElementException(identifier);
    } else {
      SessionImpl s = (SessionImpl) o;
      s.access();
      return s;
    }
  }//getSession

  /**
   * Ends the session by removing the references and running
   * the notifier of the listeners.
   * NOTE: This method should only be called by a session itself on
   * invalidation.
   *
   * @param s the {@link Session}.
   * @param l a <tt>CountDownLatch</tt>.
   */
  public void endSession(Session s, CountDownLatch l) {
    if (m_Sessions.remove(s.getIdentifier()) != null) {
      m_Services.getExecutionService(5000).execute(m_ServiceAgentProxy.getAuthenticPeer(), new Notifier((SessionImpl) s, l));
    }
  }//endSession

  private SessionImpl findAgentSession(AgentIdentifier aid) {
    for (Iterator iterator = m_Sessions.entrySet().iterator(); iterator.hasNext();) {
      Map.Entry e = (Map.Entry) iterator.next();
      SessionImpl s = (SessionImpl) e.getValue();
      if (s.getAgent().getAgentIdentifier().equals(aid)) {
        return s;
      }
    }
    return null;
  }//findAgentSession

  public String getNewSessionId() {
    return m_Services.getUUIDGeneratorService(ServiceMediator.WAIT_UNLIMITED).getUID();
  }//getNewSessionId

  private synchronized void scheduleEvictor() {
    if (m_EvictorFuture == null) {
      m_EvictorFuture =
          m_Services.getExecutionService(ServiceMediator.WAIT_UNLIMITED).scheduleAtFixedRate(m_ServiceAgentProxy.getAuthenticPeer(), new Evictor(), 0, 60, TimeUnit.SECONDS);
      Activator.log().info(m_LogMarker, m_BundleMessages.get("SessionServiceImpl.evictor.task"));
    }
  }//scheduleEvictor

  public synchronized void doMaintenance(Agent caller)
      throws SecurityException, MaintenanceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), Maintainable.DO_MAINTENANCE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getAgent()", ex);
    }
    if (m_EvictorFuture == null) {
      //schedule new one
      Activator.log().info(m_LogMarker, m_BundleMessages.get("SessionServiceImpl.evictor.unavailable"));
      scheduleEvictor();
    } else {
      if (m_EvictorFuture.isCancelled() || m_EvictorFuture.isDone()) {
        m_EvictorFuture = null;
        scheduleEvictor();
        Activator.log().info(m_LogMarker, m_BundleMessages.get("SessionServiceImpl.evictor.broken"));
      }
    }
  }//doMaintenance

  private class Evictor
      implements Runnable {

    public void run() {
      //System.out.println("Evitor::run()");
      try {
        for (Iterator<String> iter = m_Sessions.keySet().iterator(); iter.hasNext();) {
          Session s = m_Sessions.get(iter.next());
          if (s != null) {
            ((SessionImpl) s).invalidateIfTimedOut();
          }
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "Evictor::run", ex);
      }
    }//run

  }//inner class Evictor

  private class Notifier
      implements Runnable {

    private SessionImpl m_Session;
    private CountDownLatch m_RunLatch;

    public Notifier(SessionImpl s, CountDownLatch l) {
      m_Session = s;
      m_RunLatch = l;
    }//constructor

    public void run() {
      for (Iterator iter = m_Session.getSessionListeners(); iter.hasNext();) {
        try {
          SessionListener l = (SessionListener) iter.next();
          Activator.log().debug(m_LogMarker, "Notifier::run()::About to run " + l.toString());
          l.invalidated(m_Session);
          iter.remove();
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "Notifier::run()", ex);
        }
      }
      if (m_ServiceAgentProxy.getSecurityService().invalidateAuthentication(m_Session.getAgent())) {
        Activator.log().info(m_LogMarker, "Invalidated authentication of " + m_Session.getAgent().toString());
      }
      Activator.log().info(m_LogMarker, "Ended " + m_Session.toString());
      m_RunLatch.countDown();
    }//run

  }//Notifier

  private static Action[] ACTIONS = {Maintainable.DO_MAINTENANCE, Restoreable.DO_BACKUP, Restoreable.DO_RESTORE};

}//class SessionServiceImpl
