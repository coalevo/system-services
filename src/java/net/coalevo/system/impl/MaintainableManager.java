/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.system.impl;

import net.coalevo.foundation.model.Maintainable;
import org.apache.commons.collections.list.CursorableLinkedList;
import org.osgi.framework.*;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Iterator;

/**
 * This class implements a manager for {@link Maintainable}
 * implementations registered with the container.
 * <p/>
 * The instances are handled according to the OSGi white-board model.
 * All registered instances of the {@link Maintainable} class will be
 * handled here to make them available to the {@link Maintainable}.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MaintainableManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(MaintainableManager.class.getName());

  private BundleContext m_BundleContext;
  private CursorableLinkedList m_Maintainables;
  private MaintainableListener m_MaintainableListener;

  public MaintainableManager() {
    m_Maintainables = new CursorableLinkedList();
  }//MaintainableManager

  /**
   * Activates this <tt>MaintainableManager</tt>.
   * The logic will automatically detect all {@link Maintainable}
   * class objects in the container, whether registered before or after
   * the activation (i.e. white board model implementation).
   *
   * @param bc the <tt>BundleContext</tt>.
   */
  public void activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare listener
    m_MaintainableListener = new MaintainableListener();
    //prepare the filter
    String filter = "(objectclass=" + Maintainable.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(m_MaintainableListener, filter);
      //ensure that already registered Provider instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        m_MaintainableListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
    }
  }//activate

  /**
   * Deactivates this <tt>ShellCommandProviderManagerImpl</tt>.
   * The logic will remove the listener and release all
   * references.
   */
  public void deactivate() {
    //remove the listener
    m_BundleContext.removeServiceListener(m_MaintainableListener);
    m_Maintainables.clear();

    //null out the references
    m_Maintainables = null;
    m_MaintainableListener = null;
    m_BundleContext = null;
  }//deactivate

  /**
   * Tests if a {@link Maintainable} with the given identifier
   * is available.
   *
   * @param identifier a {@link Maintainable} identifier.
   * @return true if available, false otherwise.
   */
  public boolean isAvailable(String identifier) {
    return find(identifier) != null;
  }//isAvailable

  public Iterator<Maintainable> getMaintenables() {
    return (Iterator<Maintainable>) m_Maintainables.listIterator();
  }//getMaintainables

  /**
   * Registers a {@link Maintainable} implementation.
   *
   * @param maintainable a {@link Maintainable}.
   * @return true if registered, false otherwise.
   */
  public boolean register(Maintainable maintainable) {
    final String id = maintainable.getClass().getName();
    boolean c = m_Maintainables.contains(maintainable);

    if (!c) {
      m_Maintainables.add(maintainable);
      Activator.log().info(m_LogMarker,
          Activator.getBundleMessages().get("MaintainableManager.registered", "id", id)
      );
    }
    return c;
  }//register

  /**
   * Unregisters a {@link Maintainable} implementation.
   *
   * @param maintainable a{@link Maintainable}.
   * @return true if unregistered, false otherwise.
   */
  public boolean unregister(Maintainable maintainable) {
    final String id = maintainable.getClass().getName();
    boolean c = m_Maintainables.contains(maintainable);
    if (c) {
      m_Maintainables.remove(id);
      Activator.log().info(m_LogMarker,
          Activator.getBundleMessages().get("MaintainableManager.unregistered", "id", id)
      );
    }
    return c;
  }//unregister

  private Maintainable find(String identifier) {
    for (Iterator iter = m_Maintainables.listIterator(); iter.hasNext();) {
      Maintainable m = (Maintainable) iter.next();
      if (m.getClass().getName().equals(identifier)) {
        return m;
      }
    }
    return null;
  }//find

  private class MaintainableListener
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:registered:null");
          } else if (!(o instanceof Maintainable)) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:registered:Reference not a Maintainable instance.");
          } else {
            register((Maintainable) o);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:unregistering:null");
          } else if (!(o instanceof Maintainable)) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:unregistering:Reference not a Maintainable instance.");
          } else {
            unregister((Maintainable) o);
          }
          break;
      }
    }
  }//inner class MaintainableListener

}//class MaintainableManager
